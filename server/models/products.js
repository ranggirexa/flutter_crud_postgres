// import { model, Schema } from "mongoose";

// const product = model(
//   "products",
//   Schema(
//     {
//       productName: String,
//       productDescription: String,
//       productPrice: Number,
//       productImage: String
//     },
//     { timestamps: true }
//   )
// );

// export default {
//   product,
// };

import { Sequelize } from "sequelize";
import db from "../config/Database.js";

const {DataTypes} = Sequelize;

const Product = db.define('products', {
	productName:{
		type: DataTypes.STRING
	},
	productDescription:{
		type: DataTypes.STRING
	},
	productPrice:{
		type: DataTypes.INTEGER
	},
	productImage:{
		type: DataTypes.STRING
	},
},{
	freezeTableName: true
})

export default Product