// import { product } from "../models/products.model";
import Product from "../models/products.js";


const createProduct = async (params, callback) => {
  if (!params.productName) {
    return callback(
      {
        message: "Product Name Required",
      },
      ""
    );
  }

  const productModel = new Product(params);
  productModel
    .save()
    .then((response) => {
      return callback(null, response);
    })
    .catch((error) => {
      return callback(error);
    });
}

const getProducts = async (params, callback)=> {
  const productName = params.productName;
  var condition = productName
    ? { productName: { $regex: new RegExp(productName), $options: "i" } }
    : {};

 	await Product.findAll(condition)
    .then((response) => {
      return callback(null, response);
    })
    .catch((error) => {
      return callback(error);
    });
}

// async function getProductById(params, callback) {
//   const productId = params.productId;

//   product
//     .findById(productId)
//     .then((response) => {
//       if (!response) callback("Not found Product with id " + productId);
//       else callback(null, response);
//     })
//     .catch((error) => {
//       return callback(error);
//     });
// }

// async function updateProduct(params, callback) {
//   const productId = params.productId;

//   product
//     .findByIdAndUpdate(productId, params, { useFindAndModify: false })
//     .then((response) => {
//       if (!response) callback(`Cannot update Tutorial with id=${productId}. Maybe Tutorial was not found!`);
//       else callback(null, response);
//     })
//     .catch((error) => {
//       return callback(error);
//     });
// }

// async function deleteProduct(params, callback) {
//   const productId = params.productId;

//   product
//     .findByIdAndRemove(productId)
//     .then((response) => {
//       if (!response) callback(`Cannot delete Product with id=${productId}. Maybe Product was not found!`);
//       else callback(null, response);
//     })
//     .catch((error) => {
//       return callback(error);
//     });
// }

export default {
  createProduct,
  getProducts,
//   getProductById,
//   updateProduct,
//   deleteProduct
};
