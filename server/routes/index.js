// const router = require('express').Router()
// import { Express,Router } from 'express'
import express from "express";
import product from "./product.js";

const routers = express.Router()
routers.use("/api", product)

// const articleRouter = require('./articleRouter')
// const api = require('./api')

// router.use(api)
// Router.use(articleRouter, shopRouter)
// router.use(shopRouter)
export default routers
