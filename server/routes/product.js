// const routes = require('express').Router()
// const article = require('../controllers/articleController')

// routes.get('/api/v0/article', article.list)

// module.exports = routes

import express from "express";
import {create, findAll} from "../controllers/product.js"

const product = express.Router()

product.post("/products", create);
product.get("/products", findAll);

export default product
