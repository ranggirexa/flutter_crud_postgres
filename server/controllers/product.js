// import Product from "../models/products.js";
import upload from "../middleware/upload.js";
// import createProduct from "../services/products.services.js";
import productsServices from "../services/products.services.js";

export const create = async (req, res, next) => {
	upload(req, res,  (err) => {
		if (err) {
		  next(err);
		} else {
		  const url = req.protocol + "://" + req.get("host");
	
		  const path =
			req.file != undefined ? req.file.path.replace(/\\/g, "/") : "";
	
		  var model = {
			productName: req.body.productName,
			productDescription: req.body.productDescription,
			productPrice: req.body.productPrice,
			productImage: path != "" ? url + "/" + path : "",
			// productImage: req.body.productImage,
		  };
	
		  productsServices.createProduct(model, (error, results) => {
			if (error) {
			  return next(error);
			}
			return res.status(200).send({
			  message: "Success",
			  data: results,
			});
		  });
		}
	  });
}

export const findAll = async (req, res, next) => {
	var model = {
	  productName: req.query.productName,
	};
  
	productsServices.getProducts(model, (error, results) => {
	  if (error) {
		return next(error);
	  }
	  return res.status(200).send({
		message: "Success",
		data: results,
	  });
	});
  };