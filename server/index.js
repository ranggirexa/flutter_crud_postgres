import express from "express";
import db from "./config/Database.js";
import cors from "cors";
import helmet from "helmet";
import morgan from "morgan";


// import Users from "./models/userModel.js";
// import Fields from "./models/fieldModel.js";
// import Plants from "./models/plantModel.js";
// import City from "./models/cityModel.js";
// import Products from "./models/productModel.js";
import Product from "./models/products.js";
import router from "./routes/index.js";
const app = express()
app.use(helmet());
app.use(helmet.crossOriginResourcePolicy({ policy: "cross-origin" }));
app.use(morgan("common"));
app.use(cors());

try {
	await db.authenticate()
	console.log('database connected...');
	// await City.sync()
	// await Users.sync()
	// await Fields.sync()
	// await Plants.sync()
	// await Product.sync()
} catch (error) {
	console.log('error', error);
}

app.use(express.json())
app.use(router)

app.listen(5000, () => console.log(`sever running at port 5000`))